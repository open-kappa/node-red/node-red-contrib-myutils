import type {
    IMyNode,
    OutputMessageType
} from "./myutilsImpl";
import type {
    Node,
    NodeAPI,
    NodeCredentials,
    NodeDef,
    NodeMessageInFlow,
    NodeSettings
} from "node-red";

/**
 * Helper type for node `registerType()` third parameter.
 * @typeParam TCreds - Credentials type.
 * @typeParam TSets - Properties type.
 * @see getMyBuilders()
 */
export type MyNodeOpts<
    TCreds extends Record<string, unknown>,
    TSets
> = {
    credentials?: NodeCredentials<TCreds> | undefined;
    settings?: NodeSettings<TSets> | undefined;
};

/**
 * Base type for custom node properties.
 * @typeParam TCreds - Credentials type.
 * @typeParam TSets - Properties type.
 * @see getMyBuilders()
 */
export interface TMyCustomNodeDef<
    TCreds extends Record<string, unknown>,
    TSets
>
{
    /** The name of the new node. */
    name: string;
    /** JSON object of possible options. */
    opts?: MyNodeOpts<TCreds, TSets>;
}

/**
 * Type used to constrin user classes constructors.
 * @typeParam TClass - User node class type.
 * @typeParam TProps - Properties type.
 * @typeParam TCreds - Credentials type.
 * @see getMyBuilders()
 */
export type UserClassCror<
    TClass extends IMyNode<TProps, TCreds>,
    TCreds extends Record<string, unknown>,
    TProps extends NodeDef
> = new (
    nodeRed: NodeAPI,
    node: Node<TCreds>,
    config: TProps
) => TClass;

/**
 * Type of user node parameters, which are classes.
 * @typeParam TClass - User node class type.
 * @typeParam TCreds - Credentials type.
 * @typeParam TProps - Properties type.
 * @typeParam TSets - Properties type.
 * @see getMyBuilders()
 */
export interface TMyCustomNodeClassDef<
    TClass extends IMyNode<TProps, TCreds>,
    TCreds extends Record<string, unknown> =
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    TClass extends IMyNode<infer T, infer C> ? C : Record<string, unknown>,
    TProps extends NodeDef = TClass extends IMyNode<infer T, TCreds> ?
        T : NodeDef,
    TSets = Record<string, unknown>
>
    extends TMyCustomNodeDef<TCreds, TSets>
{
    /** The class which implements the node. */
    UserClass: UserClassCror<TClass, TCreds, TProps>;
}

/**
 * Internal type for `onInput`.
 * @typeParam TCreds - Credentials type.
 * @typeParam TProps - Properties type.
 * @typeParam TSets - Properties type.
 * @param nodeRed - The nodered instance.
 * @param node - The node instance.
 * @param props - The properties from the GUI.
 * @param msg - The received message.
 * @param opts - The node options.
 * @returns The output message.
 * @see TMyCustomNodeFooDef
 */
export type OnInputType<
    TCreds extends Record<string, unknown>,
    TProps extends NodeDef,
    TSets
> = (
    nodeRed: NodeAPI,
    node: Node<TCreds>,
    props: TProps,
    msg: NodeMessageInFlow,
    opts?: MyNodeOpts<TCreds, TSets>
) => Promise<OutputMessageType>;

/**
 * Internal type for `onInput`.
 * @typeParam TCreds - Credentials type.
 * @typeParam TProps - Properties type.
 * @typeParam TSets - Properties type.
 * @param nodeRed - The nodered instance.
 * @param node - The node instance.
 * @param props - The properties from the GUI.
 * @param isRemoved - Whether the node is removed.
 * @param opts - The node options.
 * @returns A completion promise.
 * @see TMyCustomNodeFooDef
 */
export type OnCloseType<
    TCreds extends Record<string, unknown>,
    TProps extends NodeDef,
    TSets
> = (
    nodeRed: NodeAPI,
    node: Node<TCreds>,
    props: TProps,
    isRemoved: boolean,
    opts?: MyNodeOpts<TCreds, TSets>
) => Promise<void>;

/**
 * Type of user node parameters, which are functions.
 * @typeParam TCreds - Credentials type.
 * @typeParam TProps - Properties type.
 * @typeParam TSets - Properties type.
 * @see getMyBuilders()
 */
export interface TMyCustomNodeFooDef<
    TCreds extends Record<string, unknown> = Record<string, unknown>,
    TProps extends NodeDef = NodeDef,
    TSets = Record<string, unknown>
>
    extends TMyCustomNodeDef<TCreds, TSets>
{
    /** The function which implements the node. */
    onInput: OnInputType<TCreds, TProps, TSets>;
    /** Optional function which implements the onClose event. */
    onClose?: OnCloseType<TCreds, TProps, TSets>;
}

/**
 * Type guard.
 * @typeParam TCreds - Credentials type.
 * @typeParam TProps - Properties type.
 * @typeParam TSets - Properties type.
 * @param arg - The instance to be checked.
 * @returns True if it is an instance.
 */
export function isMyCustomNodeFooDef<
    TCreds extends Record<string, unknown>,
    TProps extends NodeDef,
    TSets
>(
    arg: TMyCustomNodeDef<TCreds, TSets>
): arg is TMyCustomNodeFooDef<TCreds, TProps, TSets>
{
    const value = arg as TMyCustomNodeFooDef<TCreds, TProps, TSets>;
    return typeof value.onInput !== "undefined";
}

/**
 * Type guard.
 * @typeParam TCreds - Credentials type.
 * @typeParam TProps - Properties type.
 * @typeParam TSets - Properties type.
 * @param arg - The instance to be checked.
 * @returns True if it is an instance.
 */
export function isMyCustomNodeClassDef<
    TClass extends IMyNode<TProps, TCreds>,
    TCreds extends Record<string, unknown>,
    TProps extends NodeDef,
    TSets
>(
    arg: TMyCustomNodeDef<TCreds, TSets>
): arg is TMyCustomNodeClassDef<TClass, TCreds, TProps, TSets>
{
    const value = arg as TMyCustomNodeClassDef<TClass, TCreds, TProps, TSets>;
    return typeof value.UserClass !== "undefined";
}

/**
 * Type for the configuration parameter of the cusotm node.
 * @typeParam TClass - Class type.
 * @typeParam TCreds - Credentials type.
 * @typeParam TProps - Properties type.
 * @typeParam TSets - Properties type.
 * @see getMyBuilders()
 */
export type MyCustomNodeParamDef<
    TClass extends IMyNode<TProps, TCreds>,
    TCreds extends Record<string, unknown>,
    TProps extends NodeDef,
    TSets
> =
    TMyCustomNodeClassDef<TClass, TCreds, TProps, TSets>
    | TMyCustomNodeFooDef<TCreds, TProps, TSets>;
