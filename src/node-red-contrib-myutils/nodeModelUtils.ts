import type {
    Node,
    NodeAPI,
    NodeDef
} from "node-red";
import type {
    IMyNode
} from "./myutilsImpl";

/** Constant for the internal key. */
const _NODE_MODEL_KEY = "myutilsNodeModelKey";

/**
 * Extracts TProps from a TClass parameter.
 * Internal support type to type to type `getNodeModel()`.
 * @typeParam TClass - The class type.
 * @see getNodeModel()
 */
export type TClassProps<TClass> =
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    TClass extends IMyNode<infer TProps, infer _TCreds> ? TProps : NodeDef;

/**
 * Extracts TCreds from a TClass parameter.
 * Internal support type to type to type `getNodeModel()`.
 * @see getNodeModel()
 */
export type TClassCreds<TClass> =
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    TClass extends IMyNode<infer _TProps, infer TCreds>
        ? TCreds : Record<string, unknown>;

/**
 * Internal method to set the model on the node.
 * @typeParam TCreds - Credentials type.
 * @param node - The node-red node.
 * @param model - The model to set.
 */
export function setNodeModel<TCreds extends Record<string, unknown>>(
    node: Node<TCreds>,
    model: IMyNode<NodeDef, TCreds>
): void
{
    const anyNode = node as unknown as Record<string, IMyNode<NodeDef, TCreds>>;
    anyNode[_NODE_MODEL_KEY] = model;
}

/**
 * Get the model from a node.
 * @typeParam TClass - Class type.
 * @typeParam TCreds - Credentials type.
 * @typeParam TProps - Properties type.
 * @param node - The node.
 * @returns The model instance.
 */
export function getNodeModel<
    TClass extends IMyNode<TProps, TCreds>,
    TCreds extends Record<string, unknown> = TClassCreds<TClass>,
    TProps extends NodeDef = TClassProps<TClass>,
    TNode extends Node<TCreds> = Node<TCreds>
>(
    node: TNode
): TClass
{
    const anyNode = node as unknown as Record<string, TClass>;
    return anyNode[_NODE_MODEL_KEY];
}

/**
 * Get a node by name.
 * @typeParam TCreds - Credentials type.
 * @param nodeRed - The node-red node.
 * @param nodeName - The node name.
 * @returns The model instance.
 */
export function getNode<
    TCreds extends Record<string, unknown> = Record<string, unknown>
>(
    nodeRed: NodeAPI,
    nodeName: string
): Error | Node<TCreds> | null
{
    // Actually, the configuration node could be missing, if not configured.
    const genericNode: Node | undefined = nodeRed.nodes.getNode(nodeName);
    // eslint-disable-next-line max-len
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition, @typescript-eslint/strict-boolean-expressions
    if (!genericNode) return null;
    return genericNode as unknown as Node<TCreds>;
}
