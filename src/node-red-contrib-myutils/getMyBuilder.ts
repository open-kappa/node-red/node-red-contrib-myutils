import {
    IMyNode,
    isMyCustomNodeClassDef,
    isMyCustomNodeFooDef,
    type MyCustomNodeParamDef,
    MyFooNode,
    type OutputMessageType,
    setNodeModel,
    type TMyCustomNodeDef
} from "./myutilsImpl";
import type {
    Node,
    NodeAPI,
    NodeDef,
    NodeMessageInFlow
} from "node-red";

/**
 * Build the appropriate node.
 * @typeParam TClass - Class type.
 * @typeParam TProps - Properties type.
 * @typeParam TCreds - Credentials type.
 * @typeParam TSets - Properties type.
 * @param nodeRed - The nodered instance.
 * @param node - The node instance.
 * @param config - The configuration from the GUI.
 * @param buildConfig - The build configuration.
 * @throws If the user class does not match the requirements.
 */
function _buildNode<
    TClass extends IMyNode<TProps, TCreds>,
    TProps extends NodeDef,
    TCreds extends Record<string, unknown>,
    TSets
>(
    nodeRed: NodeAPI,
    node: Node<TCreds>,
    config: TProps,
    buildConfig: TMyCustomNodeDef<TCreds, TSets>
): IMyNode<TProps, TCreds>
{
    if (isMyCustomNodeClassDef<TClass, TCreds, TProps, TSets>(buildConfig))
    {
        const data = new buildConfig.UserClass(nodeRed, node, config);
        if (!(data instanceof IMyNode))
        {
            const msg = `User type must extend 'IMyNode': ${buildConfig.name}`;
            node.error(msg);
            throw new Error(msg);
        }
        return data;
    }
    else if (isMyCustomNodeFooDef<TCreds, TProps, TSets>(buildConfig))
    {
        return new MyFooNode<TProps, TCreds, TSets>(
            nodeRed,
            node,
            config,
            buildConfig
        );
    }

    const msg = `Invalid user type: ${buildConfig.name}`;
    node.error(msg);
    throw new Error(msg);
}

/**
 * Create the function to be exported to node-red.
 * The created model is set as a field of the node, and can be retrieved by
 * using `getNodeModel()`.
 * @typeParam TSets - Properties type.
 * @typeParam TCreds - Credentials type.
 * @typeParam TClass - Class type.
 * @param buildConfig - The build configuration.
 * @throws If the user class does not match the requirements.
 */
export function getMyBuilder<
    TProps extends NodeDef,
    TCreds extends Record<string, unknown>,
    TSets,
    TClass extends IMyNode<TProps, TCreds> = IMyNode<TProps, TCreds>
>(
    buildConfig: MyCustomNodeParamDef<TClass, TCreds, TProps, TSets>
): (nodeRed: NodeAPI) => void
{
    function builder(nodeRed: NodeAPI): void
    {
        function nodeBuilder(this: Node<TCreds>, config: NodeDef): void
        {
            const self = this;
            nodeRed.nodes.createNode(self, config);

            const nodeModel = _buildNode(nodeRed, self, config, buildConfig);
            setNodeModel(self, nodeModel);

            function onError(): void
            {
                // ntd
            }
            function onInputCallback(
                msg: NodeMessageInFlow,
                send?: (msg: OutputMessageType) => void,
                done?: (err?: Error) => void
            ): void
            {
                nodeModel.onInput(msg, send, done)
                    .catch(onError);
            }

            function onCloseCallback(
                removed: boolean | (() => void),
                doneFunc: (() => void) | undefined
            ): void
            {
                nodeModel.onClose(removed, doneFunc)
                    .catch(onError);
            }

            self.on("input", onInputCallback);
            self.on("close", onCloseCallback);
        }
        nodeRed.nodes.registerType(
            buildConfig.name,
            nodeBuilder,
            buildConfig.opts
        );
    }
    return builder;
}
