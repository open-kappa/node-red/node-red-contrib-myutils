import type {
    Node,
    NodeAPI,
    NodeDef,
    NodeMessageInFlow
} from "node-red";
import {
    type OutputMessageType,
    parseOnCloseParams
} from "./myutilsImpl";

/**
 * Base class for custom nodes.
 * @typeParam TProps - Properties type.
 * @typeParam TCreds - Credentials type.
 */
export abstract class IMyNode<
    TProps extends NodeDef = NodeDef,
    TCreds extends Record<string, unknown> = Record<string, unknown>
>
{
    protected readonly _config: TProps;
    protected readonly _node: Node<TCreds>;
    protected readonly _nodeRed: NodeAPI;

    /**
     * Constructor.
     * @param nodeRed - The nodered instance.
     * @param node - The node instance.
     * @param config - The configuration from the GUI.
     */
    public constructor(
        nodeRed: NodeAPI,
        node: Node<TCreds>,
        config: TProps
    )
    {
        this._config = {...config};
        this._node = node;
        this._nodeRed = nodeRed;
    }

    /**
     * On close handler.
     * @param remove - Whether the node is removed.
     * @param doneFunc - The method to signal the completion.
     * @returns A completion promise.
     */
    public async onClose(
        removed: boolean | (() => void),
        doneFunc: (() => void) | undefined
    ): Promise<void>
    {
        const self = this;
        const {done, isRemoved} = parseOnCloseParams(removed, doneFunc);
        async function onSuccess(): Promise<void>
        {
            done();
            return Promise.resolve();
        }
        async function onError(_err: Error): Promise<void>
        {
            done();
            return Promise.resolve();
        }
        async function impl(): Promise<void>
        {
            return self.onCloseImpl(isRemoved);
        }
        return impl()
            .then(onSuccess)
            .catch(onError);
    }

    /**
     * On input handler.
     * @param msg - The received message.
     * @param send - The method to send the result.
     * @param done - The method to signal the completion.
     * @returns A completion promise.
     */
    public async onInput(
        msg: NodeMessageInFlow,
        send?: (msg: OutputMessageType) => void,
        done?: (err?: Error) => void
    ): Promise<void>
    {
        const self = this;
        async function onSuccess(
            outMsg: OutputMessageType | null
        ): Promise<void>
        {
            const wrappedSend =
                send
                ?? ((outMsgRes: OutputMessageType): void =>
                {
                    self._node.send(outMsgRes);
                });
            if (outMsg !== null) wrappedSend(outMsg);
            if (typeof done !== "undefined") done();
            return Promise.resolve();
        }
        async function onError(err: Error): Promise<void>
        {
            const isError = err.message !== "";
            if (typeof done !== "undefined")
            {
                // Node-RED 1.0 compatible
                if (isError) done(err);
                else done();
            }
            else if (isError)
            {
                // Node-RED 0.x compatible
                self._node.error(err, msg);
            }
            return Promise.resolve();
        }
        async function impl(): Promise<OutputMessageType | null>
        {
            return self.onInputImpl(msg);
        }
        return impl()
            .then(onSuccess)
            .catch(onError);
    }

    /**
     * On input handler default implementation.
     * @remarks
     * Should be reimplemented by child classes.
     * This default implementation rejects with an empty error.
     * Rejecting with an empty error is just a way to complete the promises
     * chain, without generating an actual error.
     * @param _removed - Whether the node is removed.
     * @returns The output message, or null.
     */
    protected async onCloseImpl(
        _removed: boolean
    ): Promise<void>
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
        return Promise.resolve();
    }

    /**
     * On input handler default implementation.
     * @remarks
     * Should be reimplemented by child classes.
     * This default implementation rejects with an empty error.
     * @param _msg - The input message.
     * @returns The output message.
     */
    protected async onInputImpl(
        _msg: NodeMessageInFlow
    ): Promise<OutputMessageType>
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
        return Promise.reject(new Error());
    }
}
