import type {
    NodeMessage
} from "node-red";

/**
 * Support type for output messages.
 */
export type OutputMessageType =
    Array<Array<NodeMessage> | NodeMessage | null> | NodeMessage;
