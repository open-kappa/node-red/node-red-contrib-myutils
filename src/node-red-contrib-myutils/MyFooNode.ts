import {
    IMyNode,
    type MyNodeOpts,
    type OutputMessageType,
    type TMyCustomNodeFooDef
} from "./myutilsImpl";
import type {
    Node,
    NodeAPI,
    NodeDef,
    NodeMessageInFlow
} from "node-red";

/**
 * The class for custom nodes implemented by using functions.
 * @typeParam TProps - Properties type.
 * @typeParam TCreds - Credentials type.
 * @typeParam TSets - Settings type.
 */
export class MyFooNode<
    TProps extends NodeDef,
    TCreds extends Record<string, unknown>,
    TSets
>
    extends IMyNode<TProps, TCreds>
{
    private readonly _buildConfig: MyNodeOpts<TCreds, TSets>;

    /** The on close callback implementation */
    private readonly _onClose: (
        nodeRed: NodeAPI,
        node: Node<TCreds>,
        props: TProps,
        isRemoved: boolean,
        opts?: MyNodeOpts<TCreds, TSets>
    ) => Promise<void>;

    /** The on input callback implementation */
    private readonly _onInput: (
        nodeRed: NodeAPI,
        node: Node<TCreds>,
        props: TProps,
        msg: NodeMessageInFlow,
        opts?: MyNodeOpts<TCreds, TSets>
    ) => Promise<OutputMessageType>;

    /**
     * Constructor.
     * @param nodeRed - The nodered instance.
     * @param node - The node instance.
     * @param config - The configuration from the GUI.
     * @param buildConfig - The configuration from the custom node.
     */
    public constructor(
        nodeRed: NodeAPI,
        node: Node<TCreds>,
        config: TProps,
        buildConfig: TMyCustomNodeFooDef<TCreds, TProps, TSets>
    )
    {
        super(nodeRed, node, config);
        this._buildConfig = {...buildConfig.opts};
        this._onInput = buildConfig.onInput;
        this._onClose = buildConfig.onClose
            ?? (async(): Promise<void> => {return Promise.resolve();});
    }

    protected override async onCloseImpl(
        removed: boolean
    ): Promise<void>
    {
        const self = this;
        return self._onClose(
            self._nodeRed,
            self._node,
            self._config,
            removed,
            self._buildConfig
        );
    }

    protected override async onInputImpl(
        msg: NodeMessageInFlow
    ): Promise<OutputMessageType>
    {
        const self = this;
        return self._onInput(self._nodeRed, self._node, self._config, msg);
    }
}
