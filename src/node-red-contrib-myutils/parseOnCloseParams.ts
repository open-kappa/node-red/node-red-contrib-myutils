/**
 * Return type of parseOnCloseParameters().
 * @see parseOnCloseParameters()
 */
export type OnCloseParams = {
    /** The node-red done() callback. */
    // eslint-disable-next-line @typescript-eslint/method-signature-style
    readonly done: () => void;
    /** Whether the node is removed. */
    readonly isRemoved: boolean;
};

/**
 * Parse "on close" method parameters, in order to make them safe
 * across node-red versions.
 * @param removed - The node removed flag.
 * @param doneFunc - The function callback.
 * @returns The sanitized couple of input parameters.
 */
export function parseOnCloseParams(
    removed: boolean | (() => void),
    doneFunc: (() => void) | undefined
): OnCloseParams
{
    function doneFoo(): void
    {
        // ntd
    }
    let done = doneFoo;
    let isRemoved = true;

    if (typeof removed === "function")
    {
        isRemoved = true;
        done = removed;
    }
    else if (typeof removed === "boolean")
    {
        isRemoved = removed;
        if (typeof doneFunc === "function")
        {
            done = doneFunc;
        }
    }

    return {
        "done": done,
        "isRemoved": isRemoved
    };
}
