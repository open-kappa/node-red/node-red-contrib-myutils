/* eslint-disable max-classes-per-file */
import "should";
import {
    getMyBuilder,
    IMyNode,
    type OutputMessageType,
    type TMyCustomNodeClassDef,
    type TMyCustomNodeFooDef
} from "../node-red-contrib-myutils/index";
import {
    MyNodeRedTest,
    type TestFlowsItem
} from "@open-kappa/mytest";
import type {
    Node,
    NodeAPI,
    NodeDef,
    NodeMessage,
    NodeMessageInFlow
} from "node-red";


class _PippoNode
    extends IMyNode
{
    public constructor(
        nodeRed: NodeAPI,
        node: Node,
        config: NodeDef
    )
    {
        super(nodeRed, node, config);
    }

    protected override async onCloseImpl(
        _isRemoved: boolean
    ): Promise<void>
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
        return Promise.resolve();
    }

    protected override async onInputImpl(
        msg: NodeMessageInFlow
    ): Promise<OutputMessageType>
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
        return Promise.resolve(msg);
    }
}

async function _plutoOnClose(
    _nodeRed: NodeAPI,
    _node: Node,
    _props: NodeDef,
    _isRemoved: boolean
): Promise<void>
{
    return Promise.resolve();
}

async function _plutoOnInput(
    _nodeRed: NodeAPI,
    _node: Node,
    _props: NodeDef,
    msg: NodeMessageInFlow
): Promise<OutputMessageType>
{
    return Promise.resolve(msg);
}


const _DISNEY_PIPPO: TMyCustomNodeClassDef<_PippoNode> = {
    "name": "pippoNode",
    "UserClass": _PippoNode
    // "opts": unassigned in this example
};

const _DISNEY_PLUTO: TMyCustomNodeFooDef = {
    "name": "plutoNode",
    "onClose": _plutoOnClose,
    "onInput": _plutoOnInput
    // "opts": unassigned in this example
};

const _PIPPO_BUILDER = getMyBuilder(_DISNEY_PIPPO);
const _PLUTO_BUILDER = getMyBuilder(_DISNEY_PLUTO);

class _ThisTest
    extends MyNodeRedTest
{
    public constructor()
    {
        super("myutils test", [_PIPPO_BUILDER, _PLUTO_BUILDER]);
    }

    protected registerFlows(): void
    {
        const self = this;
        self.registerFlow(
            "Nodes builders",
            // eslint-disable-next-line @typescript-eslint/unbound-method
            self._flowBuilders
        );
        self.registerFlow(
            "Class nodes I/O",
            // eslint-disable-next-line @typescript-eslint/unbound-method
            self._flowInputClass
        );
        self.registerFlow(
            "Function nodes I/O",
            // eslint-disable-next-line @typescript-eslint/unbound-method
            self._flowInputFoo
        );
    }

    protected registerTests(): void
    {
        const self = this;
        self.registerTest(
            "Nodes builders",
            self._testBuilders.bind(self)
        );
        self.registerTest(
            "Class nodes I/O",
            self._testInputClass.bind(self)
        );
        self.registerTest(
            "Function nodes I/O",
            self._testInputFoo.bind(self)
        );
    }

    private _flowBuilders(): Array<TestFlowsItem>
    {
        const self = this;
        return [
            self.makeNode("n0", "Pippo", "pippoNode"),
            self.makeNode("n1", "Pluto", "plutoNode")
        ];
    }

    private _flowInputClass(): Array<TestFlowsItem>
    {
        const self = this;

        /* eslint-disable function-call-argument-newline */
        const ret = self.makeNodeWithSourceAndSink(
            "n0", "node0",
            "n1", "node1", "pippoNode",
            "n2", "node2"
        );
        /* eslint-enable function-call-argument-newline */

        return ret;
    }

    private _flowInputFoo(): Array<TestFlowsItem>
    {
        const self = this;

        /* eslint-disable function-call-argument-newline */
        const ret = self.makeNodeWithSourceAndSink(
            "n0", "node0",
            "n1", "node1", "plutoNode",
            "n2", "node2"
        );
        /* eslint-enable function-call-argument-newline */

        return ret;
    }

    private _testBuilders(): void
    {
        const self = this;
        const n0 = self.getNode("n0");
        n0.should.have.property("name", "Pippo");
        const n1 = self.getNode("n1");
        n1.should.have.property("name", "Pluto");
    }

    private _testInputClass(done: (err?: Error) => void): void
    {
        const self = this;
        const n1 = self.getNode("n1");
        const n2 = self.getNode("n2");
        type _Msg = NodeMessage & {"payload"?: {"tableName": string}};
        function checkResult(msg: NodeMessageInFlow): void
        {
            try
            {
                ((msg as _Msg).payload?.tableName === "test_table")
                    .should.be.true("Class node I/O not working");
                done();
            }
            catch (error)
            {
                done(error as Error);
            }
        }
        n2.on("input", checkResult);
        const msg: _Msg = {
            "payload": {"tableName": "test_table"}
        };
        n1.receive(msg);
    }

    private _testInputFoo(done: (err?: Error) => void): void
    {
        const self = this;
        const n1 = self.getNode("n1");
        const n2 = self.getNode("n2");
        type _Msg = NodeMessage & {"payload"?: {"tableName": string}};
        function checkResult(msg: NodeMessageInFlow): void
        {
            try
            {
                ((msg as _Msg).payload?.tableName === "test_table")
                    .should.be.true("Function node I/O not working");
                done();
            }
            catch (error)
            {
                done(error as Error);
            }
        }
        n2.on("input", checkResult);
        const msg: _Msg = {
            "payload": {"tableName": "test_table"}
        };
        n1.receive(msg);
    }
}


const _TEST = new _ThisTest();
_TEST.run();
