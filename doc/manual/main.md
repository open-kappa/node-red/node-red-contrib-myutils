![node-red-contrib-myutils logo](node-red-contrib-myutils.png)

This package provides a simple utility library, to simplify node-red custom
nodes creation.

# Links

* Project homepage: [hosted on GitLab Pages](
  https://open-kappa.gitlab.io/node-red/node-red-contrib-myutils)

* Project sources: [hosted on gitlab.com](
  https://gitlab.com/open-kappa/node-red/node-red-contrib-myutils)

# License

*@open-kappa/node-red-contrib-myutils* is released under the liberal MIT
License. Please refer to the LICENSE.txt project file for further details.

# Patrons

This node-red module has been sponsored by [Gizero Energie s.r.l.](
www.gizeroenergie.it).
