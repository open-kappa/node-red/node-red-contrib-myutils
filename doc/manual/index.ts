/* eslint-disable */

/**
 * @module -- Manual --
 */

export class Manual
{
    private constructor()
    {}

    /**
     * [[include:1.motivations.md]]
     */
    "1. Motivations": void;

    /**
     * [[include:2.creating_nodes.md]]
     */
    "2. Creating nodes": void;

    /**
     * [[include:3.utilities.md]]
     */
     "3. Utilities": void;

    /**
     * [[include:4.contributing.md]]
     */
    "4. Contributing": void;

    /**
     * [[include:5.copyright.md]]
     */
    "5. Copyright": void;

    /**
     * [[include:6.changelog.md]]
     */
    "6. Changelog": void;
}
