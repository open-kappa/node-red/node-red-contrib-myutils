#!/bin/bash

function get_script_dir()
{
    local DIR=""
    local SOURCE="${BASH_SOURCE[0]}"
    while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
        DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
        SOURCE="$(readlink "$SOURCE")"
        [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
    done
    THIS_SCRIPT_DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
}
get_script_dir

function build_json()
{
    local NODE_NAME="$1"
    if [ -d src/$NODE_NAME/json ]; then
        cp -r src/$NODE_NAME/json dist/$NODE_NAME
        if [ "$?" != "0" ]; then
            exit 1
        fi
    fi
}

function build_node()
{
    local NODE_NAME="$1"
    mkdir -p dist/src/$NODE_NAME
    build_json $NODE_NAME
}

cd $THIS_SCRIPT_DIR/..
build_node node-red-contrib-myutils
build_node tests

# EOF
